# frozen_string_literal: true

# Класс, представляющий геометрическую фигуру.
class Shape
  # Метод для вычисления площади фигуры.
  #
  # @return [Float] Площадь фигуры.
  def area; end
end

# Класс, представляющий круг, унаследованный от Shape.
class Circle < Shape
  # Атрибуты:
  # - radius: Радиус круга.

  attr_accessor :radius

  # Инициализация нового объекта класса Circle.
  #
  # @param [Float] radius Радиус круга.
  def initialize(radius)
    super()
    @radius = radius
  end

  # Метод для вычисления площади круга.
  #
  # @return [Float] Площадь круга.
  def area
    Math::PI * @radius**2
  end

  # Метод для возвращения имени фигуры.
  #
  # @return [String] Имя круга.
  def name
    'Круг'
  end
end

# Класс, представляющий прямоугольник, унаследованный от Shape.
class Rectangle < Shape
  # Атрибуты:
  # - side_a: Длина одной стороны прямоугольника.
  # - side_b: Длина второй стороны прямоугольника.

  attr_accessor :side_a, :side_b

  # Инициализация нового объекта класса Rectangle.
  #
  # @param [Float] side_a Длина одной стороны прямоугольника.
  # @param [Float] side_b Длина второй стороны прямоугольника.
  def initialize(side_a, side_b)
    super()
    @side_a = side_a
    @side_b = side_b
  end

  # Метод для вычисления площади прямоугольника.
  #
  # @return [Float] Площадь прямоугольника.
  def area
    side_a * side_b
  end

  # Метод для возвращения имени фигуры.
  #
  # @return [String] Имя прямоугольника.
  def name
    'Прямоугольник'
  end
end

# Класс, представляющий треугольник, унаследованный от Shape.
class Triangle < Shape
  # Атрибуты:
  # - side_a: Длина первой стороны треугольника.
  # - side_b: Длина второй стороны треугольника.
  # - side_c: Длина третьей стороны треугольника.

  attr_accessor :side_a, :side_b, :side_c

  # Инициализация нового объекта класса Triangle.
  #
  # @param [Float] side_a Длина первой стороны треугольника.
  # @param [Float] side_b Длина второй стороны треугольника.
  # @param [Float] side_c Длина третьей стороны треугольника.
  def initialize(side_a, side_b, side_c)
    super()
    @side_a = side_a
    @side_b = side_b
    @side_c = side_c
  end

  # Метод для вычисления площади треугольника.
  #
  # @return [Float] Площадь треугольника.
  def area
    p = (side_a + side_b + side_c) / 2.0
    Math.sqrt(p * (p - side_a) * (p - side_b) * (p - side_c))
  end

  # Метод для возвращения имени фигуры.
  #
  # @return [String] Имя треугольника.
  def name
    'Треугольник'
  end
end

# Пример использования классов:

# Создание объектов каждого класса.
circle1 = Circle.new(3)
rectangle1 = Rectangle.new(2, 5)
triangle1 = Triangle.new(3, 4, 5)

# Создание массива фигур.
shape_array = [circle1, rectangle1, triangle1]

# Вывод информации о каждой фигуре (имя и площадь).
shape_array.each do |shape|
  puts "#{shape.name} имеет площадь = #{shape.area}"
end
