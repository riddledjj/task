# frozen_string_literal: true

# Класс, представляющий книгу.
class Book
  # Атрибуты:
  # - title: Название книги.
  # - author: Автор книги.
  # - pages: Количество страниц в книге.

  attr_accessor :title, :author, :pages

  # Инициализация нового объекта класса Book.
  #
  # @param [String] title Название книги.
  # @param [String] author Автор книги.
  # @param [Integer] pages Количество страниц в книге.
  def initialize(title, author, pages)
    @title = title
    @author = author
    @pages = pages
  end

  # Метод для отображения информации о книге.
  def display_info
    puts "Название книги #{@title}, автор #{@author}, количество страниц #{@pages}"
  end
end

# Класс, представляющий электронную книгу, унаследованный от Book.
class Ebook < Book
  # Атрибуты:
  # - format: Формат электронной книги.

  attr_accessor :format

  # Инициализация нового объекта класса Ebook.
  #
  # @param [String] title Название книги.
  # @param [String] author Автор книги.
  # @param [Integer] pages Количество страниц в книге.
  # @param [String] format Формат электронной книги.
  def initialize(title, author, pages, format)
    super(title, author, pages)
    @format = format
  end

  # Метод для отображения информации о электронной книге.
  def display_info
    super
    puts "Формат книги: #{format}"
  end
end

# Класс, представляющий бумажную книгу, унаследованный от Book.
class PaperBook < Book
  # Атрибуты:
  # - weight: Вес бумажной книги.

  attr_accessor :weight

  # Инициализация нового объекта класса PaperBook.
  #
  # @param [String] title Название книги.
  # @param [String] author Автор книги.
  # @param [Integer] pages Количество страниц в книге.
  # @param [Integer] weight Вес бумажной книги.
  def initialize(title, author, pages, weight)
    super(title, author, pages)
    @weight = weight
  end

  # Метод для отображения информации о бумажной книге.
  def display_info
    super
    puts "Вес книги: #{weight} грамм"
  end
end

# Класс, представляющий библиотеку.
class Library
  # Инициализация нового объекта класса Library.
  def initialize
    @books = []
  end

  # Метод для добавления книги в библиотеку.
  #
  # @param [Book] book Добавляемая книга.
  def add_book(book)
    @books << book
  end

  # Метод для отображения информации о всех книгах в библиотеке.
  def display_books
    @books.each(&:display_info)
  end
end

# Пример использования классов:

# Создание объектов каждого класса.
ebook1 = Ebook.new('Ночной дозор', 'Сергей Лукьяненко', 320, 'PDF')
ebook2 = Ebook.new('Дневной дозор', 'Сергей Лукьяненко', 350, 'EPUB')
ebook3 = Ebook.new('Сумеречный дозор', 'Сергей Лукьяненко', 290, 'PDF')
paper_book4 = PaperBook.new('Преступление и наказание', 'Фёдор Достоевский', 420, 550)
paper_book5 = PaperBook.new('Мертвые души', 'Николай Гоголь', 250, 325)

# Создание объекта библиотеки.
all_books = Library.new

# Добавление книг в библиотеку.
all_books.add_book(ebook1)
all_books.add_book(ebook2)
all_books.add_book(ebook3)
all_books.add_book(paper_book4)
all_books.add_book(paper_book5)

# Отображение информации о всех книгах в библиотеке.
all_books.display_books
