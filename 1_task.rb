# frozen_string_literal: true

# Класс, представляющий студента.
class Student
  # Атрибуты:
  # - name: Имя студента.
  # - grade: Класс (или курс), в котором учится студент.
  # - marks: Массив, содержащий оценки студента.

  attr_accessor :name, :grade, :marks

  # Инициализация нового объекта класса Student.
  #
  # @param [String] name Имя студента.
  # @param [String] grade Класс (или курс), в котором учится студент.
  # @param [Integer] mark Первая оценка студента, добавляемая в массив оценок.
  def initialize(name, grade, mark)
    @name = name
    @grade = grade
    @marks = [mark]
  end

  # Метод для добавления новой оценки в массив оценок студента.
  #
  # @param [Integer] mark Новая оценка для добавления.
  def add_mark(mark)
    @marks.push(mark)
  end

  # Метод для вычисления средней оценки студента.
  #
  # Выводит на экран сообщение с именем студента и средней оценкой.
  def average_mark
    sum_result = @marks.sum
    average = sum_result.to_f / @marks.count
    puts "Средняя оценка для #{name}: #{average}"
  end
end

# Пример использования класса Student:

# Создание двух объектов класса Student.
student1 = Student.new('Vadym', '9', 5)
student2 = Student.new('Laring', '8', 5)

# Добавление оценок для первого студента.
student1.add_mark(3)
student1.add_mark(3)
student1.add_mark(3)
student1.add_mark(5)
student1.add_mark(5)
student1.add_mark(5)
student1.add_mark(5)
student1.add_mark(3)

# Добавление оценок для второго студента.
student2.add_mark(5)
student2.add_mark(5)
student2.add_mark(5)
student2.add_mark(3)

# Вычисление и вывод средней оценки для каждого студента.
student1.average_mark
student2.average_mark
