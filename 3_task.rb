# frozen_string_literal: true

# Класс, представляющий персонажа.
class Character
  # Атрибуты:
  # - name: Имя персонажа.
  # - health: Здоровье персонажа.

  attr_accessor :name, :health

  # Инициализация нового объекта класса Character.
  #
  # @param [String] name Имя персонажа.
  # @param [Integer] health Здоровье персонажа.
  def initialize(name, health)
    @name = name
    @health = health
  end

  # Метод для вывода информации о персонаже.
  def info
    puts "Имя: #{@name}, Здоровье: #{@health}"
  end
end

# Класс, представляющий воина, унаследованный от Character.
class Warrior < Character
  # Атрибуты:
  # - weapon: Оружие воина.

  attr_accessor :weapon

  # Инициализация нового объекта класса Warrior.
  #
  # @param [String] name Имя воина.
  # @param [Integer] health Здоровье воина.
  # @param [String] weapon Оружие воина.
  def initialize(name, health, weapon)
    super(name, health)
    @weapon = weapon
  end

  # Метод для выполнения атаки воина.
  def attack
    puts "Воин #{@name} атакует с #{@weapon}!"
  end
end

# Класс, представляющий мага, унаследованный от Character.
class Mage < Character
  # Атрибуты:
  # - element: Магический элемент, связанный с заклинаниями мага.

  attr_accessor :element

  # Инициализация нового объекта класса Mage.
  #
  # @param [String] name Имя мага.
  # @param [Integer] health Здоровье мага.
  # @param [String] element Магический элемент мага.
  def initialize(name, health, element)
    super(name, health)
    @element = element
  end

  # Метод для произнесения заклинания мага.
  def cast_spell
    puts "Маг #{@name} произносит заклинание с элементом #{@element}!"
  end
end

# Пример использования классов:

# Создание объектов каждого класса.
riddle = Warrior.new('Вадим', 100, 'Меч')
laring = Mage.new('Андрей', 95, 'Проклятье')

# Создание массива персонажей.
all_characters = [riddle, laring]

# Вывод информации о каждом персонаже и выполнение соответствующих действий.
all_characters.each do |character|
  character.info

  if character.is_a?(Warrior)
    character.attack
  elsif character.is_a?(Mage)
    character.cast_spell
  end
end
